"""
usage: bulk_customer_import.py [-h] [-l LOGLEVEL]
                               base_url auth_user auth_pass filename
                               servicedesk_id

positional arguments:
  base_url              The url of the hosted instance of JIRA https://yourdomain.atlassian.net
  auth_user             Username for basic http authentication (Account needs to be jira admin)
  auth_pass             Password for basic http authentication
  filename              The filepath to the CSV. CSV is assumed to have a header row. Columns ordered Organisation, Full Name, Email Address
  servicedesk_id        The id of the service desk e.g https://<base_url>/servicedesk/customer/portal/2  <-- the '2' is the ID

optional arguments:
  -h, --help            show this help message and exit
  -l LOGLEVEL, --loglevel LOGLEVEL
                        Set log level (DEBUG,INFO,WARNING,ERROR,CRITICAL)

example:
https://ur-base.atlassian.net/servicedesk/customer/portal/1
  python bulk_customer_import.py "https://mycustomer.atlassian.net" "local-admin" "P4ssw0rd" customers.csv 2 -l debug

CSV Format: (CSV is assumed to have a header row)
  Organisation Name, Customer Full Name, Customer Email
  Apple, Steve Jobs, steve.jobs@apple.com
  Microsoft, Bill Gates, bill.gates@microsoft.com
"""

import requests, json, logging, argparse, csv
import sys
from urllib.parse import urlencode, urlsplit

parser = argparse.ArgumentParser()
parser.add_argument( "base_url", help="The url of the hosted instance of JIRA")
parser.add_argument( "auth_user", help="Username for basic http authentication")
parser.add_argument( "auth_pass", help="Password for basic http authentication")
parser.add_argument( "filename", help="The issuetype name of the initiative")
parser.add_argument( "servicedesk_id", help="The id of the service desk")
parser.add_argument( "-l", "--loglevel", help="Set log level (DEBUG,INFO,WARNING,ERROR,CRITICAL)")
args = parser.parse_args()

# Initialize logging
logger = logging.getLogger(__name__)
logger.setLevel(getattr(logging, args.loglevel.upper() if args.loglevel else "INFO"))
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
handler=logging.StreamHandler()
handler.setFormatter(formatter)
logger.addHandler(handler)

jira_session = None
api_url = args.base_url + "/rest/servicedeskapi"

rows_not_processed = []

def init():
    # init session
    global jira_session
    jira_session = get_session(args.base_url, args.auth_user, args.auth_pass)

def parse_csv(filename):
    output = []
    with open(filename, 'r') as f:
        reader = csv.reader(f, delimiter=',', quotechar='"', skipinitialspace=True)
        for row in reader:
            output.append(row)
    return output

def get_session(base_url, auth_user, auth_pass):
    #init session
    logger.info("Initializing session")
    session = requests.Session()
    session.auth = (auth_user, auth_pass)
    url = args.base_url + "/rest/auth/1/session"
    response = session.get(url)
    logger.debug("{} ({}) - [{}] {}".format(response.status_code, response.reason, "GET", url))

    if response.ok:
        return session

    logger.error("Session initialization falied with status {} ({}).".format(response.status_code, response.reason))
    sys.exit()

def add_customer_to_servicedesk(servicedesk_id, customer):
    headers = {
        "X-ExperimentalApi" : "opt-in",
        "Content-Type": "application/json"
    }
    fields = { "email":  customer["emailAddress"],
                "displayName": customer["fullName"]}
    url = api_url + "/customer"
    response = jira_session.post(url, headers=headers, data=json.dumps(fields))

    logger.debug("{} ({}) - [{}] {}".format(response.status_code, response.reason, "POST", url))
    print(customer["emailAddress"])
    if response.ok:
        logger.info("{} was successfully created".format(customer["emailAddress"]))
        return json.loads(response.text)

    logger.error(response.text)
    return False

def create_customer(customer):
    logger.debug(customer)
    headers = {
        "X-ExperimentalApi" : "true",
        "Content-Type": "application/json"
    }
    payload = {
        "name": customer["fullName"],
        "emailAddress": customer["emailAddress"],
        "displayName": customer["fullName"],
        "notification" : "true"
    }

    url = args.base_url + "/rest/api/2/user"
    response = jira_session.post(url, headers=headers, data=json.dumps(payload))
    logger.debug("{} ({}) - [{}] {}".format(response.status_code, response.reason, "POST", url))

    if response.ok:
        logger.info("{} was successfully created".format(customer["emailAddress"]))
        return json.loads(response.text)

    logger.error(response.text)
    return False

def main():
    global rows_not_processed
    rows = parse_csv(args.filename)
    for row in rows[1:]: #ignore header and run through csv from second row(actual data)
        try:
            organization_name, customer_name, customer_email = row[0], row[1], row[2]

            # Create the customer if they do not already exist
            new_customer = { "fullName": customer_name, "emailAddress": customer_email}
            # create_customer(new_customer)
            add_customer_to_servicedesk(args.servicedesk_id, new_customer)
        except:
            logger.exception("Failed to process row: {}".format(row))
            rows_not_processed.append(row)

    logger.info("The following rows not processed")

    for row in rows_not_processed:
        print(row)

if __name__ == "__main__":
    init()
    main()
